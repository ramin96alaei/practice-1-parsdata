import { createApp } from 'vue';
import App from './App.vue';
import '@/assets/css/app.scss';
import Carousel3d from 'vue-carousel-3d';
const app = createApp(App);
app.use(Carousel3d)
app.mount('#app');
