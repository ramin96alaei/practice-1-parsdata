module.exports = {
  purge: ['./src/**/*.html', './src/**/*.js'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      primary: '#019CD5',
      accent: '#FD3D39',
      secondary: '#324b4b',
      info: '#cc8fe2',
      warning: '#eee8a9',
      error: '#8a1d00',
      success: '#005e3b',
      white: '#ffffff',
      black: '#222222',
      gray: {
        dark: '#797877',
        light: '#E5E5E5',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
